declare class MultiProgress {
  constructor(writeStream: NodeJS.WriteStream);
}
declare module 'multi-progress' {
  export default MultiProgress;
}